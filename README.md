# SVARA Radio API

Programming Language: Javascript  
Framework : Node JS Express  
Database : MongoDB  

To run project, do following steps:  
    
    1. Create `.env` files  
    2. Copy all contents on `.env.examples` and set as your configuration  
    3. Open terminal in your working dir  
    4. run command `npm init`  
    5. run command `npm run start_dev` to run on development environment or `npm start` to run on your production environment  
  
  
For documentation, please refer to link below:  
[SVARA RADIO REST API DOCUMENTATION](https://documenter.getpostman.com/view/2106914/T17NaQMw)