const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

require('dotenv').config();
require('./src/config/database');

const PORT = process.env.APP_PORT || 3000;
app.use(bodyParser.json());
app.use(cors());

const routes = require('./src/routes/route');
routes(app);

app.listen(PORT, () => {
  console.log(`server listening on port ${PORT}`);
});
