const Radio = require('../Models/Radio');
const resp = require('../../config/response');

const get = (req, res) => {
  return Radio.find({})
  .then(radios => {
    return resp(res, 200, radios);
  })
  .catch(err => {
    console.log(err);
    return resp(res, 500, "There was a problem finding the radio.");
  });
}

const getById = (req, res) => {
  const id = req.params.id;

  return Radio.findById(id)
  .then(radio => {
    if (!radio) return resp(res, 404, "No radio found.");
    return resp(res, 200, radio);
  })
  .catch(err => {
    console.log(err);
    return resp(res, 500, "There was a problem finding the radio.");
  });
}

const create = (req, res) => {

  const {name, city, frequency, logo, stream, website} = req.body;

  return Radio.create({
    name,
    city,
    frequency,
    logo,
    stream,
    website
  })
  .then(radio => {
    if (!radio) return resp(res, 404, "No radio found.");
    return resp(res, 201, radio)
  })
  .catch(err => {
    console.log(err)
    return resp(res, 500, "There was a problem adding the information to the database.");
  });
}

const update = (req, res) => {
  const id = req.params.id;
  const {name, city, frequency, logo, stream, website} = req.body;

  return Radio.findByIdAndUpdate(id, 
    {
      name,
      city,
      frequency,
      logo,
      stream,
      website
    },
    {
      new: true
    })
    .then(radio => {
      return resp(res, 200, radio);
    })
    .catch(err => {
      return resp(res, 500, "There was a problem updating the radio.");
    })
}

const remove = (req, res) => {
  const id = req.params.id;

  return Radio.findByIdAndRemove(id)
  .then(radio => {
    if (!radio) return resp(res, 404, "No radio found.");
    resp(res, 204, "Radio: "+ radio.name +" was deleted.");
  })
  .catch(err => {
    console.log(err)
    resp(res, 500, "There was a problem deleting the radio.");
  });
}


module.exports = {
  get,
  getById,
  create,
  update,
  remove
}