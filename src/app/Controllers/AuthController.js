const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const resp = require('../../config/response');
const User = require('../Models/User');

const register = (req, res) => {
  const hashedPassword = bcrypt.hashSync(req.body.password, 8);
  
  User.create({
    email : req.body.email,
    password : hashedPassword
  })
  .then((user) => {
    const token = jwt.sign({ id: user._id, email: user.email, guest_mode: false }, process.env.APP_SECRET, {
      expiresIn: process.env.TOKEN_EXPIRES || 86400 // expires in 24 hours
    });
    return resp(res, 200, {auth: true, token: token});
  }).catch(err => {
    if(err._message == "user validation failed"){
      return resp(res, 400, "Email already registered");
    }
    console.log(err);
    return resp(res, 400, "Email already registered");
  });
}

const login = (req, res) => {

  const { email, password } = req.body;

  if(email && password){
    return User.findOne({ email })
    .then(user => {
      if(!user){
        return resp(res, 401, 'Invalid email or password.');
      }

      const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid) {
        return resp(res, 401, 'Invalid email or password.');
      }
      
      var token = jwt.sign({ id: user._id, email: user.email, guest_mode: false }, process.env.APP_SECRET, {
        expiresIn: process.env.TOKEN_EXPIRES || 86400
      });

      return resp(res, 200, { token: token }).send();
    })
    .catch(err => {
      console.log(err);
      return res.status(500).send('Error on the server.');
    });
  }


  
  return resp(res, 400, "Email or Password not provided");
};

const guestMode = (req, res) => {
  var token = jwt.sign({ guest_mode: true }, process.env.APP_SECRET, {
    expiresIn: process.env.TOKEN_EXPIRES || 86400
  });

  return resp(res, 200, { token: token }).send();
  
};

module.exports = {
  register,
  login,
  guestMode
}