const mongoose = require('mongoose');
const radioSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  frequency: {
    type: String,
    required: false
  },
  logo: {
    type: String,
    required: true
  },
  stream: {
    type: String,
    required: true
  },
  website: {
    type: String,
    required: false
  }
}, {versionKey: false});
module.exports = mongoose.model('radio', radioSchema);