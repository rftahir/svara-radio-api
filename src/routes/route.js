const verifyToken = require('../middleware/VerifyToken');
const VerifyGuestMode = require('../middleware/VerifyGuestMode');
const Auth = require('../app/Controllers/AuthController');
const Radios = require('../app/Controllers/RadioController');


module.exports = (app) => {
  app.get('/', (req, res) => {
    res.send("Svara REST API v1.0 by Rizky Tahir");
  });

  app.post('/users/register', Auth.register);
  app.post('/users/login', Auth.login);
  app.get('/users/guest-mode', Auth.guestMode);

  app.use('/radios', [verifyToken])
  app.get('/radios', Radios.get);
  app.get('/radios/:id', Radios.getById);
  app.post('/radios', VerifyGuestMode, Radios.create);
  app.put('/radios/:id', VerifyGuestMode, Radios.update);
  app.delete('/radios/:id', VerifyGuestMode, Radios.remove);
};