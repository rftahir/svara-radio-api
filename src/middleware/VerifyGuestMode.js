const resp = require('../config/response');

function verifyToken(req, res, next) {
    if(req.guest_mode){
      resp(res, 401, "You are in guest mode and not allowed to access this endpoint.");
    }

    next();
}

module.exports = verifyToken;