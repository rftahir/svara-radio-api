const jwt = require('jsonwebtoken');
const resp = require('../config/response');

function verifyToken(req, res, next) {
  const token = req.headers['x-access-token'];
  if (!token){
    return resp(res, 403, { auth: false, message: 'No token provided.' });
  }
    
  jwt.verify(token, process.env.APP_SECRET, function(err, decoded) {
    if (err){
      return resp(res, 500, { auth: false, message: 'Failed to authenticate token.' });
    }

    req.guest_mode = decoded.guest_mode;

    next();
  });
}

module.exports = verifyToken;