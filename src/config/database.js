const mongoose = require('mongoose');
const connectionURI = `mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}?authSource=admin`;

mongoose.Promise = global.Promise;

mongoose.connect(connectionURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false  }).then(() => {
                    console.log('Database Connected');
                  }).catch(err => {
                    console.log('Can\'t connect to database ');
                    console.log('Error on: ', err);
                    process.exit();
                  })